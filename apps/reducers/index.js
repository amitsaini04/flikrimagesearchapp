import {combineReducers} from 'redux'
import HomeReducer from '../screens/home/reducer'
const rootReducer = combineReducers({
    home : HomeReducer
})

export default rootReducer;