import React from 'react'
import { View, TextInput, FlatList, Image, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native'
import styles from './Styles'
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const imageContainerWidth = deviceWidth / 4
const HomeRender = ({ setSearchKeyword, search, searchResults, searchKeyword, openCarousel, isCarouselEnabled, closeCarousel,
    selectedImageIndex, isSearchTermChanged, isLoading }) => {
   
    return (
        <View style={styles.container}>
            <View style={isCarouselEnabled ? styles.carouselHeader : styles.header}>
                {isCarouselEnabled ?
                    <TouchableOpacity style={styles.backButtonContainer} onPress={() => closeCarousel()}>
                        <Image resizeMode="contain" source={{ uri: 'leftarrow' }} style={styles.backButton} />
                    </TouchableOpacity>
                    : <TextInput
                        style={styles.searchBar}
                        placeholder="Search"
                        value={searchKeyword}
                        onChangeText={(text) => setSearchKeyword(text)}
                        onSubmitEditing={() => search(false)}
                        returnKeyType="search"
                    />}
            </View>
            <View style={styles.body}>
                {searchKeyword != "" && searchResults.photos != null && !isSearchTermChanged ?
                    <FlatList
                        style={styles.body}
                        pagingEnabled={isCarouselEnabled}
                        numColumns={isCarouselEnabled ? undefined : 4}
                        key={isCarouselEnabled}
                        keyExtractor={item => item.id}
                        horizontal={isCarouselEnabled}
                        getItemLayout={(data, index) => (isCarouselEnabled ?
                            { length: deviceWidth, offset: deviceWidth * index, index }
                            : { length: imageContainerWidth, offset: imageContainerWidth * index, index })}
                        initialScrollIndex={isCarouselEnabled ? selectedImageIndex : Math.floor(selectedImageIndex / 4 - 3)}
                        data={searchResults.photos}
                        onEndReached={() => search(true)}
                        renderItem={({ item, index }) =>
                            isCarouselEnabled ?
                                <View style={styles.carouselImageContainer}>
                                    <Image resizeMode="contain" source={{ uri: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_z.jpg` }} style={styles.carouselImage} />
                                </View> :
                                <TouchableOpacity style={styles.imageContainer} onPress={() => openCarousel(index)}>
                                    <Image resizeMode="stretch" source={{ uri: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_m.jpg` }} style={styles.image} />
                                </TouchableOpacity>

                        }
                    />
                    : isLoading ? <View style={styles.spinnerContainer}>
                        <ActivityIndicator size="large" color="#7C7C7C" />
                    </View> :
                        <View>
                        </View>}
            </View></View>
    )
}

export default HomeRender