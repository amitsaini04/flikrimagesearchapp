import { StyleSheet, Dimensions } from 'react-native'
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    header: {
        height: deviceHeight / 10,
        backgroundColor: '#7C7C7C',
        padding: 10
    },
    body: {
        height: deviceHeight * 0.9,
        flex: 1
    },
    container: {
        flex: 1
    },
    searchBar: {
        paddingLeft: 30,
        fontSize: 18,
        flex: 1,
        backgroundColor: 'white'
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        width: deviceWidth / 4.2,
        height: deviceWidth / 4
    },
    image: {
        flexDirection: "row",
        justifyContent: "space-around",
        width: deviceWidth / 4.5,
        height: deviceWidth / 4.5
    },
    carouselImageContainer: {
        backgroundColor: 'black',
        width: deviceWidth,
        paddingLeft: 5,
        paddingRight: 5
    },
    carouselImage: {
        flex: 1
    },
    backButton: {
        width: deviceWidth / 20,
        height: deviceHeight / 30,
        marginLeft: 15
    },
    backButtonContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    carouselHeader: {
        height: deviceHeight / 10,
        backgroundColor: 'black',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    spinnerContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
})

export default styles