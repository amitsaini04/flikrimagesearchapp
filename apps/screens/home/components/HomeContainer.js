import React, { Component } from 'react'
import { connect } from 'react-redux'
import HomeRender from './HomeRender'
import * as actions from '../actions'
import {InteractionManager} from 'react-native'
import { bindActionCreators } from 'redux';
class HomeContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKeyword: "",
            isCarouselEnabled: false,
            selectedImageIndex: 0,
            isSearchTermChanged: false,
            isLoading:false
        }
    }


    setSearchKeyword = (searchKeyword) => {
        this.setState({ searchKeyword:searchKeyword,isSearchTermChanged:true,selectedImageIndex:0 })
    }

    search = (requestNextPage) => {
        let searchKeyword = this.state.searchKeyword;
        let allSearchResults = this.props.searchResults;
        let currentSearchResults = this.props.currentSearchResult;
        let totalPageNumber = null;
        if (searchKeyword == "") {
            return;
        }
        let pageNumber = 1;
        //Increase page number in case of next page request
        if (requestNextPage) {
            pageNumber = currentSearchResults.page + 1;
            totalPageNumber =  currentSearchResults.pages;
        }
        //If search term was searched earlier and same page is requested then use previous search result
        if (allSearchResults[searchKeyword] != null && allSearchResults[searchKeyword].page >= pageNumber) {
            this.props.actions.updateCurrentSearchTerm(searchKeyword)
            this.setState({isSearchTermChanged:false})
        }
        else {
            if(pageNumber == 1){
                //Show loader when first page is requested
                this.setState({isLoading:true})
            }
            //Don't fetch result if requested page number is greater than total page number
            if(totalPageNumber != null && pageNumber > totalPageNumber){
                return;
            }
            this.props.actions.getSearchResult(searchKeyword, pageNumber, (resultCount) => {
                if (resultCount == 0) {
                    //Show alert when no result is found
                    alert("No result found.")
                }
                else {
                    //Flag to indicate whether search term is changed after search 
                    this.setState({isSearchTermChanged:false})
                }
                //Turn off loader
                this.setState({isLoading:false})
            }, () => {
                this.setState({isLoading:false})
                alert("Something went wrong.")
            })
        }
    }

     //Function used to close carousel using state variable
    closeCarousel = () => {
        this.setState({ isCarouselEnabled: false })
    }

    //Function used to open carousel using state variable
    openCarousel = (index) => {
        //Set carousel enabled and save selected image index
        this.setState({ isCarouselEnabled: true, selectedImageIndex: index })
    }

    render() {
        return (
            <HomeRender setSearchKeyword={this.setSearchKeyword} search={this.search}
                searchResults={this.props.currentSearchResult} searchKeyword={this.state.searchKeyword}
                openCarousel={this.openCarousel} isCarouselEnabled={this.state.isCarouselEnabled}
                closeCarousel={this.closeCarousel} selectedImageIndex={this.state.selectedImageIndex}
                isLoading = {this.state.isLoading} isSearchTermChanged= {this.state.isSearchTermChanged}
                 />
        );
    }
}

function mapStateToProps(state) {
    return {
        searchResults: state.home.searchResults ? state.home.searchResults : {},
        currentSearchResult: state.home.searchResults ? state.home.searchResults[state.home.currentSearchKeyword] : {}
    }
}

function mapDispatchToProps(dispatch) {
    const actionsToBind = Object.assign({}, actions)
    return {
        actions: bindActionCreators(actionsToBind, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)