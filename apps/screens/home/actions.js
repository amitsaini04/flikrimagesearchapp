
import axios from 'axios'
import * as actionTypes from './actionTypes'
const API_KEY = "cdd997f4b04d47029a05da1772eca199"
const ROOT_URL = ` https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&per_page=50&format=json&nojsoncallback=1`;

function getResponse(response) {
    return {
        type: actionTypes.GET,
        payload: response
    }
}

export function getSearchResult(searchKeyword, pageNumber, sucesscb, errorcb) {
    return (dispatch) => {
        const url = `${ROOT_URL}&page=${pageNumber}&text=${searchKeyword}`;
        axios.get(url)
            .then((response) => {
                if (response.data.photos.pages != 0) {
                    let searchResultObject = {
                        searchKeyword: searchKeyword,
                        result: response.data.photos
                    }
                    dispatch(getResponse(searchResultObject))
                }
                sucesscb(response.data.photos.total);
            })
            .catch(
                //Error response
                () => {
                    errorcb();
                })
    }
}

export function updateCurrentSearchTerm(searchTerm){
    return {
        type: actionTypes.UPDATE_SEARCH_TERM,
        payload: searchTerm
    }
}