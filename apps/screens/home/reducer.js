import * as actionTypes from './actionTypes'
export default function (state = {}, action) {
    switch (action.type) {
        case actionTypes.GET:
            return Object.assign({}, state, {
                currentSearchKeyword: action.payload.searchKeyword,
                searchResults: Object.assign({}, state.searchResults ? state.searchResults : {}, {
                    [action.payload.searchKeyword]: {
                        pages: action.payload.result.pages,
                        page: action.payload.result.page,
                        photos: state.searchResults && state.searchResults[action.payload.searchKeyword] ?
                            state.searchResults[action.payload.searchKeyword].photos.concat(action.payload.result.photo) :
                            action.payload.result.photo
                    }
                }
                )
            });
        case actionTypes.UPDATE_SEARCH_TERM: return Object.assign({}, state, { currentSearchKeyword: action.payload })
    }
    return state;
}