/** @format */

import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk'
import React, {Component} from 'react';
import App from './App';
import {name as appName} from './app.json';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './apps/reducers'

export default class ReactApp extends Component {

    render(){
        const createStoreWithMiddleWare = applyMiddleware(ReduxThunk)(createStore);
        return (
            <Provider store={createStoreWithMiddleWare(rootReducer)}>
                <App />
            </Provider>
        );
    }
}

AppRegistry.registerComponent(appName, () => ReactApp);
