import React, { Component } from 'react';
import HomeContainer from './apps/screens/home/components'


class App extends Component {
  render() {
    return (
      <HomeContainer />
    );
  }
}

export default App